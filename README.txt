=== Plugin Name ===
Contributors: resonator
Tags: generator, pages, seo
Requires at least: 5.2
Tested up to: 5.2
Stable tag: 5.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Used to generate local landing pages for the RME sites.

== Description ==

This plugin allows for the easy creation of local landing pages.

The list of available cities will be automatically populated from the cities found in the store locator locations CPT.

Heroes characters can be added upon request.

Once a page is created, it can be easily edited in Pages.

== Installation ==

1. Upload `local-landing-pages.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 2.3.1 =
* CSS fix for ul and ol

= 2.3.0 =
* Removed GeoGratis API call

= 2.2.7 =
* Bugfix, too few placefolders for $wpdb->prepare in public

= 2.2.6 =
* Removed old mfn_builder_print function, replaced with the_content
* margin-top css to accommodate content

= 2.1.1 =
* Only load metabox when local landing pages template is selected.

= 2.1.0 =
* Added metabox to edit after being generated

= 2.0.1 =
* Small CSS change to center the header image.

= 2.0.0 =
* Added the generator. It's alive!

= 1.1.1 =
* Default to 10 locations if city has no locations or very few.

= 1.1.0 =
* Added TGM activator to make sure WPSL is present.

= 1.0.1 =
* Testing BitBucket update integration.

= 1.0 =
* First release candidate.

== Upgrade Notice ==