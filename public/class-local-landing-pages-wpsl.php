<?php

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Local_Landing_Pages
 * @subpackage Local_Landing_Pages/public
 * @author     Resonator <cameron@resonator.ca>
 */
class Local_Landing_Pages_WPSL {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

    }

    public function llp_wpsl_custom_templates( $templates ) {

		/**
		 * The 'id' is for internal use and must be unique ( since 2.0 ).
		 * The 'name' is used in the template dropdown on the settings page.
		 * The 'path' points to the location of the custom template,
		 * in this case the folder of your active theme.
		 */
		$templates[] = array (
			'id'   => 'local-landing-page',
			'name' => 'Local Landing Page',
			'path' => plugin_dir_path( __FILE__ ) . 'partials/wpsl-template-local-landing-page.php',
		);
	
		return $templates;
		
	}

}