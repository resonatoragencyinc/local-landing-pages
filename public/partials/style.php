<?php
/**
 * Dynamic style sheet
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://resonator.ca
 * @since      1.0.0
 *
 * @package    Local_Landing_Pages
 * @subpackage Local_Landing_Pages/includes
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

/* ==============================================================================================================================
/*	Location Credit
/* ============================================================================================================================ */

.page-template-template-local-landing-page #Subheader::before {
    content: '<?php echo get_post_meta( get_the_ID(), 'llp_header_location', true ) ?>';
}

.page-template-template-local-landing-page #Subheader .container::before {
    background-image: url("<?php echo plugin_dir_url( __FILE__ ) ?>../images/<?php echo get_post_meta( get_the_ID(), 'llp_page_hero', true ) ?>");
}

