<?php

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Local_Landing_Pages
 * @subpackage Local_Landing_Pages/public
 * @author     Resonator <cameron@resonator.ca>
 */
class Local_Landing_Pages_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Local_Landing_Pages_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Local_Landing_Pages_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/local-landing-pages-public.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Local_Landing_Pages_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Local_Landing_Pages_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/local-landing-pages-public.js', array( 'jquery' ), $this->version, false);
    }

    /**
     * Undocumented function
     *
     * @param   string  $template   The page's base template
     * @return  void
     * @since   1.0.0
     */
    public function llp_redirect_page_template($template)
    {

        $post = get_post();
        $page_template = get_post_meta($post->ID, '_wp_page_template', true);

        if ('template-local-landing-page.php' === basename($page_template)) {
            $template = plugin_dir_path(__FILE__) . 'partials/template-local-landing-page.php';
        }

        return $template;
    }

    /**
     * Echo the dynamic styles for the page
     *
     * @return  void
     * @since   1.0.0
     */
    public function llp_styles_dynamic()
    {

        echo '<style id="llp-dnmc-style-css">'."\n";
            ob_start();

            include_once plugin_dir_path(__FILE__) . 'partials/style.php';

            $css = ob_get_contents();
            ob_get_clean();

            echo Local_Landing_Pages_Helpers::llp_styles_minify( $css ) ."\n";
        echo '</style>'."\n";
    }

    public function llp_locs_in_city( $post_id )
    {

        $city = get_post_meta( $post_id, 'llp_page_city', true );

        if (empty($city)) {
            return 10;
        }

        $transient = get_transient( 'locs_in_' . $city );

        if ( ! empty( $transient ) ) {
            return $transient;
        }

        $args = array(
            'meta_key' => 'wpsl_city',
            'meta_value' => $city,
            'post_type' => 'wpsl_stores',
            'post_status' => 'publish',
            'posts_per_page' => -1
        );

        $numoflocs = count( get_posts( $args ) );
        $transient = ( $numoflocs >= 10 ) ? $numoflocs : 10;

        set_transient( 'locs_in_' . $city, $transient, HOUR_IN_SECONDS );

        return $transient;
    }

    public function llp_number_of_locations_sql( $sql )
    {

        global $wpdb, $wpsl, $wpsl_settings;

        $post_id = url_to_postid(wp_get_referer());
        $page_template = get_post_meta($post_id, '_wp_page_template', true);

        if ( 'template-local-landing-page.php' !== basename( $page_template ) ) {
            return $sql;
        }

        if ( ! isset($_GET['autoload']) && ! $_GET['autoload'] ) {
            return $sql;
        }

        if ( $wpsl->i18n->wpml_exists() ) {
            $group_by = 'GROUP BY lat';
        } else {
            $group_by = 'GROUP BY posts.ID';
        }

        $radius = get_post_meta( $post_id, 'llp_search_radius', true );
        $sql_sort = 'HAVING distance < ' . $radius  . ' ORDER BY distance LIMIT 0, %d';

        // The default query that selects store location that fall within the selected radius.
        $sql = "SELECT 	post_lat.meta_value AS lat,
						post_lng.meta_value AS lng,
						posts.ID,
						( %d * acos( cos( radians( %s ) ) * cos( radians( post_lat.meta_value ) ) * cos( radians( post_lng.meta_value ) - radians( %s ) ) + sin( radians( %s ) ) * sin( radians( post_lat.meta_value ) ) ) )
					AS distance
				FROM $wpdb->posts AS posts
				INNER JOIN $wpdb->postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'wpsl_lat'
				INNER JOIN $wpdb->postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'wpsl_lng'
				WHERE posts.post_type = 'wpsl_stores'
					AND posts.post_status = 'publish' $group_by $sql_sort";

        return $sql;
    }
}
