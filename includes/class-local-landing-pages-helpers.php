<?php

/**
 * Define the functions used between the public and admin sides. All functions are static.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Local_Landing_Pages
 * @subpackage Local_Landing_Pages/includes
 * @author     Resonator <cameron@resonator.ca>
 */
class Local_Landing_Pages_Helpers {

	/**
	 * Minify CSS
	 *
	 * @param	string	$css	Raw CSS
	 * @return	string	$css	Minified CSS
	 * @since	1.0.0
	 */
	public static function llp_styles_minify( $css ){

		// remove comments
		$css = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css );

		// remove whitespace
		$css = str_replace( array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css );

		return $css;

	}
	
}