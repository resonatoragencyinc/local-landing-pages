<?php

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Local_Landing_Pages
 * @subpackage Local_Landing_Pages/includes
 * @author     Resonator <cameron@resonator.ca>
 */
class Local_Landing_Pages {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Local_Landing_Pages_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'PLUGIN_NAME_VERSION' ) ) {
			$this->version = PLUGIN_NAME_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'local-landing-pages';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Local_Landing_Pages_Loader. Orchestrates the hooks of the plugin.
	 * - Local_Landing_Pages_i18n. Defines internationalization functionality.
	 * - Local_Landing_Pages_Admin. Defines all hooks for the admin area.
	 * - Local_Landing_Pages_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-local-landing-pages-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-local-landing-pages-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-local-landing-pages-admin.php';

		/**
		 * The class responsible for creating the settings page that occurs in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-local-landing-pages-settings-page.php';

		/**
		 * The class responsible for defining the list of local landing pages.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-local-landing-pages-list-table.php';

		/**
		 * The class responsible for the metabox on page edit.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-local-landing-pages-metabox.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-local-landing-pages-public.php';

		/**
		 * The class responsible for defining all actions that occur in the WPSL plugin
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-local-landing-pages-wpsl.php';

		/**
		 * The class responsible for defining all static helper functions.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-local-landing-pages-helpers.php';

		$this->loader = new Local_Landing_Pages_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Local_Landing_Pages_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Local_Landing_Pages_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Local_Landing_Pages_Admin( $this->get_plugin_name(), $this->get_version() );
		$plugin_settings = new Local_Landing_Pages_Settings_Page( $this->get_plugin_name(), $this->get_version() );
		$plugin_metabox = new Local_Landing_Pages_MetaBox( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'tgmpa_register', $plugin_admin, 'llp_register_required_plugins' );

		$this->loader->add_filter( 'theme_page_templates', $plugin_admin, 'llp_add_page_template' );

		$this->loader->add_action( 'admin_menu', $plugin_settings, 'llp_create_settings' );
		$this->loader->add_action( 'admin_init', $plugin_settings, 'llp_setup_sections' );
		$this->loader->add_action( 'admin_init', $plugin_settings, 'llp_setup_fields' );
		$this->loader->add_action( 'admin_init', $plugin_settings, 'llp_generator_fields' );
		$this->loader->add_action( 'admin_footer', $plugin_settings, 'media_fields' );

		$this->loader->add_action( 'wp_loaded', $plugin_settings, 'llp_add_new_page' );

		$this->loader->add_action( 'add_meta_boxes', $plugin_metabox, 'add_metabox' );
		$this->loader->add_action( 'save_post', $plugin_metabox, 'save_metabox', 10, 2 );
		
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Local_Landing_Pages_Public( $this->get_plugin_name(), $this->get_version() );
		$plugin_wpsl = new Local_Landing_Pages_WPSL( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$this->loader->add_filter( 'page_template', $plugin_public, 'llp_redirect_page_template' );
		$this->loader->add_action( 'wp_head',  $plugin_public, 'llp_styles_dynamic' );

		$this->loader->add_filter( 'wpsl_templates', $plugin_wpsl, 'llp_wpsl_custom_templates' );

		$this->loader->add_filter( 'wpsl_sql', $plugin_public, 'llp_number_of_locations_sql' );
		
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Local_Landing_Pages_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
