<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://resonator.ca
 * @since             1.0.0
 * @package           Local_Landing_Pages
 *
 * @wordpress-plugin
 * Plugin Name:       Local Landing Pages
 * Plugin URI:        https://resonator.ca
 * Description:       Local Landing Page Generator
 * Version:           2.3.1
 * Author:            Resonator
 * Author URI:        https://resonator.ca
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       local-landing-pages
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '2.3.1' );

/**
* Composer autoload
*/
require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

/**
 * Automatically check for updates
 */
$my_update_checker = Puc_v4_Factory::buildUpdateChecker(
	'https://resonator.ca/wp-update/?action=get_metadata&slug=local-landing-pages', // Metadata URL.
	__FILE__, // Full path to the main plugin file.
	'local-landing-pages' // Plugin slug. Usually it's the same as the name of the directory.
);

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-local-landing-pages-activator.php
 */
function activate_local_landing_pages() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-local-landing-pages-activator.php';
	Local_Landing_Pages_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-local-landing-pages-deactivator.php
 */
function deactivate_local_landing_pages() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-local-landing-pages-deactivator.php';
	Local_Landing_Pages_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_local_landing_pages' );
register_deactivation_hook( __FILE__, 'deactivate_local_landing_pages' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-local-landing-pages.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_local_landing_pages() {
	$plugin = new Local_Landing_Pages();
	$plugin->run();
}
run_local_landing_pages();
