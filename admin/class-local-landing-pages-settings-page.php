<?php

/**
 * The generator and generator settings page
 *
 * @since      1.2.1
 * @package    Local_Landing_Pages
 * @subpackage Local_Landing_Pages/admin
 * @author     Resonator <cameron@resonator.ca>
 */
class Local_Landing_Pages_Settings_Page
{

    /**
     * The ID of this plugin.
     *
     * @since    1.2.1
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.2.1
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * The url of this plugin page.
     *
     * @since    1.2.1
     * @access   private
     * @var      string    $page_url    The url of this plugin page.
     */
    private $page_url;

    /**
     * The list of pages.
     *
     * @since    1.2.1
     * @access   private
     * @var      string    $llp_list_table    The list of pages.
     */
    private $llp_list_table;

    /**
     * Initialize the class and set its properties.
     *
     * @since       1.2.1
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version )
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }


    public function llp_create_settings()
    {

        $parent_slug    = 'edit.php?post_type=page';
        $page_title     = 'Local Landing Page Generator';
        $menu_title     = 'Local Landing Pages';
        $capability     = 'edit_pages';
        $slug           = 'local-landing-pages';
        $callback       = array( $this, 'llp_settings_content' );

        add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $slug, $callback );

        $this->page_url = menu_page_url( 'local-landing-pages', false );

    }

    public function llp_settings_content()
    {

        $active_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'generator';

        ?> <div class="wrap">

            <h1><?php _e( 'Local Landing Page Generator', 'local-landing-page' ) ?></h1>

            <?php settings_errors(); ?>

            <h2 class="nav-tab-wrapper">
                <a href="<?php echo $this->page_url ?>&tab=generator" class="nav-tab"><?php _e( 'Generator', 'local-landing-page' ) ?></a>
                <a href="<?php echo $this->page_url ?>&tab=defaults" class="nav-tab"><?php _e( 'Generator Defaults', 'local-landing-page' ) ?></a>
                <a href="<?php echo $this->page_url ?>&tab=pages" class="nav-tab"><?php _e( 'Existing Pages', 'local-landing-page' ) ?></a>
            </h2>

            <?php if ( $active_tab == 'generator' ) { ?>
                <form method="POST" action="">
                    <?php
                        settings_fields( 'Local_Landing_Pages_Generator' );
                        do_settings_sections( 'Local_Landing_Pages_Generator');
                        submit_button( 'Generate Page', 'primary', 'generate_page' );
                    ?>
                </form>
            <?php } ?>

            <?php if ( $active_tab == 'defaults' ) { ?>
                <form method="POST" action="options.php">
                    <?php
                        settings_fields( 'Local_Landing_Pages_Settings' );
                        do_settings_sections( 'Local_Landing_Pages_Settings' );
                        submit_button();
                    ?>
                </form>
            <?php } ?>

            <?php if ( $active_tab == 'pages' ) { ?>
                <?php
                    $this->llp_list_table = new Local_Landing_Pages_List_Table( $this->plugin_name);
                    $this->llp_list_table->prepare_items();
                ?>
                <div class="wrap">
                    <div id="nds-wp-list-table-demo">
                        <div id="nds-post-body">
                            <form id="nds-user-list-form" method="get">
                                <?php
                                    $this->llp_list_table->display();
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div> <?php

    }

    public function llp_setup_sections()
    {

        add_settings_section( 'Local_Landing_Pages_settings', '', array(), 'Local_Landing_Pages_Settings' );
        add_settings_section( 'Local_Landing_Pages_generator', '', array(), 'Local_Landing_Pages_Generator' );

    }

    public function llp_setup_fields()
    {

        $fields = array(
            array(
                'label'         => __( 'Province', 'local-landing-page' ),
                'id'            => 'llp_page_province',
                'type'          => 'select',
                'section'       => 'Local_Landing_Pages_settings',
                'options'       => self::llp_get_provinces(),
                'desc'          => '',
                'required'      => true
            ),
            array(
                'label'         => __( 'Page Template', 'local-landing-page' ),
                'id'            => 'llp_template',
                'type'          => 'wysiwyg',
                'section'       => 'Local_Landing_Pages_settings',
                'desc'          => 'This is the default template used for a Local Landing Page. Use {{ city }} in place of the actual city name.',
                'required'      => false
            ),
            array(
                'label'         => __( 'Default Search Radius', 'local-landing-page' ),
                'id'            => 'llp_search_radius',
                'type'          => 'number',
                'section'       => 'Local_Landing_Pages_settings',
                'placeholder'   => '10',
                'desc'          => 'The default search radius.',
                'required'      => false
            ),
            array(
                'label'         => __( 'Default Video', 'local-landing-page' ),
                'id'            => 'llp_video_id',
                'type'          => 'text',
                'section'       => 'Local_Landing_Pages_settings',
                'placeholder'   => '',
                'desc'          => 'Enter the ID (Vimeo or YouTube) of the default video to embed',
                'required'      => false
            ),
            array(
                'label'         => __( 'Default Hero', 'local-landing-page' ),
                'id'            => 'llp_page_hero',
                'type'          => 'select',
                'section'       => 'Local_Landing_Pages_settings',
                'options'       => self::llp_get_heroes(),
                'desc'          => '',
                'required'      => false
            ),
            array(
                'label'         => __( 'Default Header Image', 'local-landing-page' ),
                'id'            => 'llp_header_image',
                'type'          => 'media',
                'section'       => 'Local_Landing_Pages_settings',
                'desc'          => 'Default header image when no other image can be used.',
                'required'      => false
            ),
            array(
                'label'         => __( 'Default Header Location', 'local-landing-page' ),
                'id'            => 'llp_header_location',
                'type'          => 'text',
                'section'       => 'Local_Landing_Pages_settings',
                'placeholder'   => '',
                'desc'          => 'Default location credit for the default header image.',
                'required'      => false
            ),
        );

        foreach ( $fields as $field) {
            add_settings_field( $field['id'], $field['label'], array( $this, 'llp_field_callback' ), 'Local_Landing_Pages_Settings', $field['section'], $field);
            register_setting( 'Local_Landing_Pages_Settings', $field['id'] );
        }

    }

    public function llp_generator_fields()
    {

        $fields = array(
            array(
                'label'         => __( 'City', 'local-landing-page' ),
                'id'            => 'llp_page_city',
                'type'          => 'text',
                'section'       => 'Local_Landing_Pages_generator',
                'placeholder'   => '',
                'desc'          => '',
                'required'      => true
            ),
            array(
                'label'         => __( 'Header Image', 'local-landing-page' ),
                'id'            => 'llp_header_image',
                'type'          => 'media',
                'section'       => 'Local_Landing_Pages_generator',
                'desc'          => '',
                'required'      => true
            ),
            array(
                'label'         => __( 'Header Location', 'local-landing-page' ),
                'id'            => 'llp_header_location',
                'type'          => 'text',
                'section'       => 'Local_Landing_Pages_generator',
                'placeholder'   => '',
                'desc'          => '',
                'required'      => true
            ),
            array(
                'label'         => __( 'Search Radius', 'local-landing-page' ),
                'id'            => 'llp_search_radius',
                'type'          => 'number',
                'section'       => 'Local_Landing_Pages_generator',
                'placeholder'   => '',
                'desc'          => '',
                'required'      => true
            ),
            array(
                'label'         => __( 'Hero', 'local-landing-page' ),
                'id'            => 'llp_page_hero',
                'type'          => 'select',
                'section'       => 'Local_Landing_Pages_generator',
                'options'       => self::llp_get_heroes(),
                'desc'          => '',
                'required'      => true
            ),
            array(
                'label'         => __( 'Page Content', 'local-landing-page' ),
                'id'            => 'llp_template',
                'type'          => 'wysiwyg',
                'section'       => 'Local_Landing_Pages_generator',
                'desc'          => __( 'Use {{ city }} in place of the actual city name', 'local-landing-page' ),
                'required'      => false
            ),
            array(
                'label'         => __( 'Video', 'local-landing-page' ),
                'id'            => 'llp_video_id',
                'type'          => 'text',
                'section'       => 'Local_Landing_Pages_generator',
                'placeholder'   => '',
                'desc'          => __( 'Enter the ID only (Vimeo or YouTube) of the video to embed', 'local-landing-page' ),
                'required'      => true
            ),
        );

        foreach ( $fields as $field ) {
            add_settings_field( $field['id'], $field['label'], array( $this, 'llp_field_callback' ), 'Local_Landing_Pages_Generator', $field['section'], $field );
            register_setting( 'Local_Landing_Pages_Generator', $field['id'] );
        }

    }

    public function llp_field_callback( $field )
    {

        $value = get_option( $field['id'] );
        switch ( $field['type'] ) {
            case 'media':
                $required = ( $field['required'] ) ? 'required' : '';

                printf(
                    '<input style="width: 40%%" id="%s" name="%s" type="text" value="%s" %s> <input style="width: 19%%" class="button Local_Landing_Pages-media" id="%s_button" name="%s_button" type="button" value="' . __( 'Add image', 'local-landing-pages' ) . '" />',
                    $field['id'],
                    $field['id'],
                    $value,
                    $required,
                    $field['id'],
                    $field['id']
                );
                break;
            case 'select':
            case 'multiselect':
                if ( ! empty( $field['options'] ) && is_array( $field['options'] ) ) {
                    $attr = '';
                    $options = '';
                    $required = ( $field['required'] ) ? 'required' : '';

                    foreach ( $field['options'] as $key => $label ) {
                        $selected = ( is_array( $value ) ) ? selected( $value[ array_search( $key, $value, true ) ], $key, false ) : '';
                        $options.= sprintf(
                            '<option value="%s" %s>%s</option>',
                            $key,
                            $selected,
                            $label
                        );
                    }

                    if ( $field['type'] === 'multiselect' ) {
                        $attr = ' multiple="multiple" ';
                    }

                    printf(
                        '<select name="%1$s[]" id="%1$s" %2$s %3$s>%4$s</select>',
                        $field['id'],
                        $attr,
                        $required,
                        $options
                    );

                    if (! empty( $field['or'] ) ) {
                        printf(
                            ' &ndash; or &ndash; <input name="%1$s2" id="%1$s2" type="%2$s" placeholder="%3$s" value="%4$s" />',
                            $field['id'],
                            $field['or'],
                            $field['placeholder'],
                            $value
                        );
                    }
                }

                break;
            case 'wysiwyg':
                wp_editor( $value, $field['id'] );
                break;
            default:
                $required = ( $field['required'] ) ? 'required' : '';

                printf(
                    '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" %5$s/>',
                    $field['id'],
                    $field['type'],
                    $field['placeholder'],
                    $value,
                    $required
                );
        }

        if ( $desc = $field['desc'] ) {
            printf('<p class="description">%s </p>', $desc);
        }

    }

    public function media_fields()
    {

        ?><script>
            jQuery(document).ready(function($){
                if (typeof wp.media !== 'undefined') {
                    var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                    $('.Local_Landing_Pages-media').click(function(e) {
                        var send_attachment_bkp = wp.media.editor.send.attachment;
                        var button = $(this);
                        var id = button.attr('id').replace('_button', '');
                        _custom_media = true;
                            wp.media.editor.send.attachment = function(props, attachment){
                            if ( _custom_media ) {
                                $('input#'+id).val(attachment.url);
                            } else {
                                return _orig_send_attachment.apply(this, [props, attachment]);
                            };
                        }
                        wp.media.editor.open(button);
                        return false;
                    });
                    $('.add_media').on('click', function(){
                        _custom_media = false;
                    });
                }
            });
        </script><?php

    }

    public static function llp_get_provinces()
    {

        $provinces = array(
            'British Columbia' => 'British Columbia',
            'Alberta' => 'Alberta',
            'Saskatchewan' => 'Saskatchewan',
            'Manitoba' => 'Manitoba',
            'Ontario' => 'Ontario',
            'Quebec' => 'Quebec',
            'New Brunswick' => 'New Brunswick',
            'Nova Scotia' => 'Nova Scotia',
            'Prince Edward Island' => 'Prince Edward Island',
            'Newfoundland and Labrador' => 'Newfoundland and Labrador',
            'Yukon' => 'Yukon',
            'Nunavut' => 'Nunavut',
            'Northwest Territories' => 'Northwest Territories',
        );

        // $provinces = array();

        // $provinces_json = get_transient( 'provinces_json' );

        // if ( empty( $provinces_json ) ) {
        //     $provinces_json = file_get_contents( 'https://geogratis.gc.ca/services/geoname/en/geonames.json?concise=PROV' );
        //     set_transient( 'provinces_json', $provinces_json, DAY_IN_SECONDS );
        // }

        // if ( ! $provinces_json ) {
        //     return;
        // }

        // $provinces_object = json_decode( $provinces_json );

        // foreach ( $provinces_object->items as $province ) {
        //     $name = $province->name;
        //     $provinces[ $name ] = $name;
        // }

        return $provinces;

    }

    public static function llp_get_cities()
    {

        $cities = array();

        $args = array(
            'post_type'         => 'wpsl_stores',
            'post_status'       => 'publish',
            'posts_per_page'    => -1
        );

        $locations = get_posts( $args );

        foreach ( $locations as $location ) {
            $city = get_post_meta( $location->ID, 'wpsl_city', true );
            $cities[ $city ] = $city;
        }

        ksort( $cities, SORT_NATURAL | SORT_FLAG_CASE );

        return array_unique( $cities );

    }

    public static function llp_get_heroes()
    {

        $heroes = array();
        $hero_images = list_files( plugin_dir_path( dirname( __FILE__ ) ) . 'public/images', 1 );

        if (! $hero_images) {
            return;
        }

        foreach ( $hero_images as $hero ) {
            if ( 'image/png' !== wp_check_filetype( $hero )['type'] ) {
                continue;
            }

            $heroes[ wp_basename( $hero) ] = wp_basename( $hero, '.png' );
        }

        return $heroes;

    }

    public function llp_add_new_page()
    {

        if (! isset( $_POST ) || ! isset( $_POST['generate_page'] ) || ! $_POST['generate_page'] ) {
            return;
        }

        $city = $_POST['llp_page_city'];

        if (get_option('llp_page_province' ) ) {
            $province = get_option('llp_page_province')[0];
        } else {
            wp_die(__('A province must be selected in Defaults', 'local-landing-page'), __('No Province Selected', 'local-landing-page'), array( 'back_link' => true  ) );
        }

        $loader = new Twig_Loader_Array( array(
            'llp' => $_POST['llp_template'],
         ) );
        $twig = new Twig_Environment( $loader );

        $content = $twig->render('llp', array(
            'city'  => $city,
            'video' => $_POST['llp_video_id']
         ) );

        $sidebar = ( array_search('Local Landing Page', mfn_opts_get('sidebars' ) ) ) ?: 0;

        $mfnbilder = array(
            array(
                'attr' => array(
                    'title' => '',
                    'bg_color' => '',
                    'bg_image' => '',
                    'bg_position' => 'no-repeat;center top;;',
                    'bg_size' => 'auto',
                    'bg_video_mp4' => '',
                    'bg_video_ogv' => '',
                    'padding_top' => '45',
                    'padding_bottom' => '10',
                    'divider' => '',
                    'decor_top' => '',
                    'decor_bottom' => '',
                    'navigation' => '',
                    'style' => '',
                    'class' => '',
                    'section_id' => '',
                    'visibility' => '',
                    'hide' => '',
                ),
                'wraps' => array(
                    array(
                        'size' => '1/1',
                        'items' => array(
                            array(
                                'type' => 'content',
                                'size' => '1/1',
                                'fields' => array(
                                    'classes' => '',
                                ),
                            ),
                        ),
                        'attr' => array(
                            'bg_color' => '',
                            'bg_image' => '',
                            'bg_position' => 'no-repeat;center top;;',
                            'bg_size' => 'auto',
                            'move_up' => '',
                            'padding' => '',
                            'column_margin' => '',
                            'vertical_align' => 'top',
                            'class' => '',
                        ),
                    ),
                ),
            ),
        );

        $args = array(
            'post_type'     => 'page',
            'post_title'    => "Electronics Recycling in {$city}",
            'post_status'   => 'draft',
            'post_content'  => $content,
            'post_name'     => strtolower( $city),
            'meta_input'    => array(
                'mfn-post-layout'       => 'right-sidebar',
                'mfn-post-sidebar'      => $sidebar,
                'mfn-post-hide-content' => '1',
                '_wp_page_template'     => 'template-local-landing-page.php',
                'mfn-page-items'        => $mfnbilder,
                'llp_header_location'   => $_POST['llp_header_location'],
                'llp_page_hero'         => $_POST['llp_page_hero'][0],
                'llp_search_radius'     => $_POST['llp_search_radius'],
                'llp_page_city'         => $city,
                'llp_page_province'     => $province
            )
        );

        $post_id = wp_insert_post( $args );
        set_post_thumbnail( $post_id, attachment_url_to_postid( $_POST['llp_header_image'] ) );

        wp_redirect( get_edit_post_link( $post_id, '' ) );
        die();

    }

}
