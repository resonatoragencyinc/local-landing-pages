<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://resonator.ca
 * @since      1.0.0
 *
 * @package    Local_Landing_Pages
 * @subpackage Local_Landing_Pages/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div id="mfn-wrapper">
    <input type="hidden" name="mfn_page_meta_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />
    <?php mfn_builder_show(); ?>
</div>
