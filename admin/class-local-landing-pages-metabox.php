<?php

class Local_Landing_Pages_Metabox
{

    /**
     * The ID of this plugin.
     *
     * @since    2.1.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    2.1.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since      2.1.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version )
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    public function add_metabox()
    {

        $page_template = ( isset( $_GET['post'] ) ) ? get_post_meta( $_GET['post'], '_wp_page_template', true ) : '';

        if ( 'template-local-landing-page.php' !== basename( $page_template ) ) {
            return;
        }

        add_meta_box( 
            'llp_page_options',
            __( 'Local Landing Page Options', 'local-landing-pages' ),
            array( $this, 'render_metabox' ),
            'page',
            'normal',
            'high'
         );

    }

    public function render_metabox( $post )
    {

        // Add nonce for security and authentication.
        wp_nonce_field( 'llp_nonce_action', 'llp_nonce' );

        $page_heroes = Local_Landing_Pages_Settings_Page::llp_get_heroes();

        // Retrieve an existing value from the database.
        $llp_page_city = get_post_meta( $post->ID, 'llp_page_city', true );
        $llp_header_location = get_post_meta( $post->ID, 'llp_header_location', true );
        $llp_page_hero = get_post_meta( $post->ID, 'llp_page_hero', true );
        $llp_search_radius = get_post_meta( $post->ID, 'llp_search_radius', true );

        // Set default values.
        if ( empty( $llp_page_city ) ) {
            $llp_page_city = '';
        }
        if ( empty( $llp_header_location ) ) {
            $llp_header_location = '';
        }
        if ( empty( $llp_page_hero ) ) {
            $llp_page_hero = '';
        }
        if ( empty( $llp_search_radius ) ) {
            $llp_search_radius = '';
        }

        // Form fields.
        echo '<table class="form-table">';

        echo '	<tr>';
        echo '		<th><label for="llp_page_city" class="llp_page_city_label">' . __( 'Page City', 'local-landing-pages' ) . '</label></th>';
        echo '		<td>';
        echo '			<input type="text" id="llp_page_city" name="llp_page_city" class="llp_page_city_field" placeholder="' . esc_attr__( '', 'local-landing-pages' ) . '" value="' . esc_attr( $llp_page_city ) . '">';
        echo '		</td>';
        echo '	</tr>';

        echo '	<tr>';
        echo '		<th><label for="llp_header_location" class="llp_header_location_label">' . __( 'Header Image Location Credit', 'local-landing-pages' ) . '</label></th>';
        echo '		<td>';
        echo '			<input type="text" id="llp_header_location" name="llp_header_location" class="llp_header_location_field" placeholder="' . esc_attr__( '', 'local-landing-pages' ) . '" value="' . esc_attr( $llp_header_location ) . '">';
        echo '		</td>';
        echo '	</tr>';

        echo '	<tr>';
        echo '		<th><label for="llp_page_hero" class="llp_page_hero_label">' . __( 'Header Hero', 'local-landing-pages' ) . '</label></th>';
        echo '		<td>';
        echo '			<select id="llp_page_hero" name="llp_page_hero" class="llp_page_hero_field">';

        foreach ( $page_heroes as $hero => $value ) {
            echo '			<option value="' . $hero . '" ' . selected( $llp_page_hero, $hero, false ) . '> ' . $value . '</option>';
        }

        echo '			</select>';
        echo '		</td>';
        echo '	</tr>';

        echo '  <tr>';
        echo '      <th><label for="llp_search_radius" class="llp_search_radius_label">' . __( 'Search Radius', 'local-landing-pages' ) . '</label></th>';
        echo '      <td>';
        echo '          <input type="number" id="llp_search_radius" name="llp_search_radius" class="llp_search_radius_field" placeholder="' . esc_attr__( '', 'local-landing-pages' ) . '" value="' . esc_attr( $llp_search_radius ) . '">';
        echo '      </td>';
        echo '  </tr>';

        echo '</table>';

    }

    public function save_metabox( $post_id, $post )
    {

        // Add nonce for security and authentication.
        $nonce_name   = isset( $_POST['llp_nonce'] ) ? $_POST['llp_nonce'] : '';
        $nonce_action = 'llp_nonce_action';

        // Check if a nonce is set.
        if ( ! isset( $nonce_name ) ) {
            return;
        }

        // Check if a nonce is valid.
        if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
            return;
        }

        // Check if the user has permissions to save data.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }

        // Check if it's not a revision.
        if ( wp_is_post_revision( $post_id ) ) {
            return;
        }

        // Sanitize user input.
        $llp_new_page_city = isset( $_POST['llp_page_city'] ) ? sanitize_text_field( $_POST['llp_page_city'] ) : '';
        $llp_new_header_location = isset( $_POST['llp_header_location'] ) ? sanitize_text_field( $_POST['llp_header_location'] ) : '';
        $llp_new_page_hero = isset( $_POST['llp_page_hero'] ) ? $_POST['llp_page_hero'] : '';
        $llp_search_radius = isset( $_POST['llp_search_radius'] ) ? sanitize_text_field( $_POST['llp_search_radius'] ) : '';

        // Update the meta field in the database.
        update_post_meta( $post_id, 'llp_page_city', $llp_new_page_city );
        update_post_meta( $post_id, 'llp_header_location', $llp_new_header_location );
        update_post_meta( $post_id, 'llp_page_hero', $llp_new_page_hero );
        update_post_meta( $post_id, 'llp_search_radius', $llp_search_radius );
        
    }
}
